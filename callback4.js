const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

module.exports = function callback(boardName, listName, boardsData, listsData, cardsData) {

    setTimeout(() => {

        callback1(boardName, boardsData, cb1 = (err, Bdata) => {
            if (err) {
                console.error(err.message);
            } else {
                console.log(Bdata);
                callback2(Bdata.id, listsData, cb2 = (err, Ldata) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        console.log(Ldata);
                        for (items of Ldata) {
                            if (items.name === 'Mind') {
                                callback3(items.id, cardsData, cb3 = (err, Cdata) => {
                                    if (err) {
                                        console.error(err)
                                    }
                                    else {
                                        console.log(Cdata);
                                    }
                                })
                            }

                        }
                    }
                })
            }
        });

    }, 2000);
};

