const callback = require('../callback3');

const path = require('path');
const cardsDataPath = path.join(__dirname, '../data/cards.json');


callback("qwsa221", cardsDataPath, (err, data) => {
    if (err) {
        console.error('Error Occurred');
        console.error(err);
    }
    else {
        console.log("Successfully got data")
        console.log(data);
    }
})
