const fs = require("fs");


module.exports = function callback(Id, cardsDataPath, cb) {

    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("Callback function is not found");
        }
        else if (typeof cardsDataPath !== "string") {
            console.error("Path not found");
        }
        else if (typeof Id !== "string") {
            console.error("ID is not correct");
        }
        else {
            fs.readFile(cardsDataPath, "utf-8", (err, cards) => {
                if (err) {
                    cb(err);
                }
                else {
                    try {
                        const card = JSON.parse(cards);
                        const cardsData = card[Id];
                        if (!cardsData) {
                            console.error(new Error("Cards not found"));
                        }
                        else {
                            cb(null, cardsData);
                        }

                    }
                    catch (err) {
                        cb(err);
                    }
                }
            });
        }

    }, 2 * 1000);

}


