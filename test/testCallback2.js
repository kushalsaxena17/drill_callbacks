const callback = require('../callback2');
const path = require('path');
const listsDataPath = path.join(__dirname, '../data/lists.json');

function cb(err, data) {
    if (err) {
        console.error('Error Occurred');
        console.error(err);
    }
    else {
        console.log('Successfully got data');
        console.log(data);

    }
}

callback("mcu453ed", listsDataPath, cb);