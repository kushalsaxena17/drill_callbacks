const fs = require("fs");


module.exports = function callback(Id, boardsDataPath, cb) {

    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("Callback function is not found");
        }
        else if (typeof boardsDataPath !== "string") {
            console.error("Path not found");
        }
        else if (typeof Id !== "string") {
            console.error("ID is not correct");
        }
        else {
            fs.readFile(boardsDataPath, 'utf-8', (err, boards) => {
                if (err) {
                    cb(err);
                }
                else {
                    try {
                        const board = JSON.parse(boards);

                        let result = board.filter((elements) => {
                            if (elements['name'] === Id || elements['id'] === Id) {
                                return elements;
                            }
                        })
                        if (result.length == 0) {
                            cb("Not data found");
                        }

                        else {
                            cb(null, result[0]);
                        }
                    }
                    catch (err) {
                        cb(err);
                    }
                }
            });
        }

    }, 2 * 1000);

}

