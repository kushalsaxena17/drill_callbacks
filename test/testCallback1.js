const callback = require('../callback1.js');
const fs = require("fs");
const path = require('path');
const boardsDataPath = path.join(__dirname, '../data/boards.json');



function cb(err, data) {
    if (err) {
        console.error('Error Occurred');
        console.error(err);
    }
    else {
        console.log('Successfully got data');
        console.log(data);

    }
}
callback("mcu453ed", boardsDataPath, cb);