const path = require("path");
const fs = require("fs");


const boardsDataPath = path.join(__dirname, "../data/boards.json");
const listsDataPath = path.join(__dirname, "../data/lists.json");
const cardsDataPath = path.join(__dirname, "../data/cards.json");

const callback = require("../callback4");


callback("Thanos", "Mind", boardsDataPath, listsDataPath, cardsDataPath);