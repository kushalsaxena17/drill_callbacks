const fs = require("fs");
const path = require("path");
const boardFilePath = path.join(__dirname, "./boards.json");
const listsFilePath = path.join(__dirname, "./lists.json");
const cardsFilePath = path.join(__dirname, "./cards.json");


module.exports = function callback(callback1, callback2, callback3, cb) {
	setTimeout(() => {
		if (typeof cb === "function") {
			try {
				callback1("Thanos", boardFilePath, (err, boards) => {
					if (err) {
						cb(err);
					} else {
						cb(null, boards);
						let boardsData = boards.find((items) => items.name === "Thanos");
						callback2(boardsData.id, listsFilePath, (err, lists) => {
							if (err) {
								cb(err);
							} else {
								cb(null, lists);
								for (let items of lists) {
									callback3(items.id, cardsFilePath, (err, cards) => {
										if (err) {
											cb(err);
										} else {
											cb(null, cards);
										}
									});
								}
							}
						});
					}
				});
			} catch (err) {
				console.error("Path is not found");
			}
		} else {
			console.error("CB is not found");
		}
	}, 2 * 1000);
}