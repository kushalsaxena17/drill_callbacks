const path = require("path");
const callback1 = require("../callback1");
const callback = require("../callback5");

function cb(err, data) {
    if (err) {
        console.error('Error Occurred');
        console.error(err);
    }
    else {
        console.log('Successfully got data');
        console.log(data);

    }
}

callback(
    "Thanos", ["Mind", "Space"], path.join(__dirname, "../data/boards.json"), path.join(__dirname, "../data/lists.json"), path.join(__dirname, "../data/cards.json")
);
