const fs = require("fs");
const callback1 = require("./callback1");
const callback2 = require("./callback2");
const callback3 = require("./callback3");
module.exports = function callback(boardName, listNames, boardsFilePath, listsFilePath, cardsFilePath) {
  setTimeout(() => {
    if (typeof boardName !== "string") {
      console.error("Name is not found");
    }
    else {
      callback1(boardName, boardsFilePath, (err, data) => {
        if (err) {
          cb(err);
        } else {
          callback2(data.id, listsFilePath, (err, data) => {
            if (err) {
              console.error(err.message);
            } else {

              const lists = data.filter((list) =>
                listNames.includes(list.name)
              );
              if (!lists) {
                console.error("List Name is not found");
              } else {
                lists.forEach((list) => {
                  callback3(list.id, cardsFilePath, (err, data) => {
                    if (err) {
                      console.error(err.message);
                    } else {
                      console.log(data);
                    }
                  }
                  );
                });
              }
            }
          });
        }
      });

    }


  }, 2000);
};