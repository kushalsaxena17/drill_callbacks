const fs = require("fs");

module.exports = function callback(Id, listsDataPath, cb) {

    setTimeout(() => {
        if (typeof cb !== "function") {
            console.error("Callback function is not found");
        }
        else if (typeof listsDataPath !== "string") {
            console.error("Path not found");
        }
        else if (typeof Id !== "string") {
            console.error("ID is not correct");
        }
        else {
            fs.readFile(listsDataPath, 'utf-8', (err, lists) => {
                if (err) {
                    cb(err);
                }
                else {
                    try {
                        const list = JSON.parse(lists);
                        const listData = list[Id];
                        if (!listData) {
                            console.error(new Error("List is not found"));
                        }
                        else {
                            cb(null, listData);
                        }

                    }
                    catch (err) {
                        cb(err);
                    }
                }
            });
        }
    }, 2 * 1000);

}
